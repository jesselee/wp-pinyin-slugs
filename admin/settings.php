<?php
/**
 * Render the Plugin options form
 * @since 2014.07.29
 * @modified 2.1.3
 */
function sops_render_form() { ?>

	<div class="wrap">

		<!-- Display Plugin Header, and Description -->
		<h1><?php _e( 'Pinyin Slugs Settings', 'so-pinyin-slugs' ); ?></h1>

		<div class="pinyinslugs-intro">

			<h3 class="hndle"><span><?php _e( 'Important Information', 'so-pinyin-slugs' ); ?></span></h3>

			<div class="inside">

				<p><?php _e( 'Pinyin Slugs automatically transforms Chinese character titles into pinyin slugs.', 'so-pinyin-slugs' ); ?></p>

				<p><?php _e( 'Pinyin Slugs will only work for new slugs, not on existing slugs. You can choose to rewrite the slugs of existing permalinks manually.', 'so-pinyin-slugs' ); ?></p>

				<p><?php _e( '<strong>Note:</strong> If you choose to manually rewrite the slugs of existing permalinks, then please keep in mind that the previous permalinks that have been indexed by search engines (like Google, Baidu) do not change automatically. This may impact your site ranking if you just leave it like that.', 'so-pinyin-slugs' ); ?></p>

				<p><?php _e( 'In that case you might want to add 301 Permanent Redirects for those specific links to your .htaccess file.', 'so-pinyin-slugs' ); ?></p>

				<p><?php _e( 'A <strong>limitation of Pinyin Slugs</strong> is that it completely ignores anything other than Chinese characters in your slug. For example: if you have a product name with a number in it, then the plugin ignores this number. You will have to add that manually to the slug if needed. Likewise, if you\'re writing an English title and you combine it with some Chinese characters, the resulting slug will only contain the pinyin of those characters; in other words the English is completely ignored.', 'so-pinyin-slugs' ); ?></p>
			</div> <!-- end .inside -->

		</div> <!-- end .pinyinslugs-intro -->

		<div id="sops-settings">

			<!-- Beginning of the Plugin Options Form -->
			<form method="post" action="options.php">

				<?php settings_fields( 'sops_plugin_options' ); ?>

				<?php $options = get_option( 'sops_options' ); ?>

				<table class="form-table"><tbody>

					<tr valign="top">
						<th scope="row">
							<label for="sops-sluglength"><?php _e( 'Slug Length', 'so-pinyin-slugs' ); ?></label>
						</th>

						<td>
							<input name="sops_options[slug_length]" type="number" id="slug_length" value="<?php echo $options['slug_length']; ?>" />
							<p class="description"><?php _e( 'By default the maximum slug length is set to 100 letters; anything over that limit will not be converted. If you want to change this limit, you can do that here.', 'so-pinyin-slugs' ); ?></p>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="page_options" value="<?php echo $options['slug_length']; ?>" />
						</td>
					</tr>

				</tbody></table> <!-- end .tbody end table -->

				<p class="submit">

					<input type="submit" class="button-primary" value="<?php _e( 'Save Settings', 'so-pinyin-slugs' ) ?>" />

				</p>

			</form>

		</div><!-- #sops-settings -->

	</div> <!-- end .wrap -->

<?php }
